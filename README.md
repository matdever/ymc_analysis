# YMC_analysis

This repository was created by M. Dever. It contains the routines used to process the data collected on the ship's rosette by an SBE CTD, and 3 RBR CTDs. It aims at looking at dynamic corrections for the CTD.

